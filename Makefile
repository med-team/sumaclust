PREFIX=/usr/local

CFLAGS=-I$(PREFIX)/include

EXEC = sumaclust

SUMACLUST_SRC = sumaclust.c \
                mtcompare_sumaclust.c

SUMACLUST_OBJ = $(patsubst %.c,%.o,$(SUMACLUST_SRC))

SRCS = $(SUMACLUST_SRC)

LIB = -lsuma -lm #-ll

include ./global.mk

all: $(EXEC)


########
#
# sumaclust compilation
#
########

# executable compilation and link

ifeq ($(CC),gcc)
        LFLAGS = -fopenmp 
else
        LFLAGS = 
endif

sumaclust: $(SUMACLUST_OBJ) $(LIBSUMA)
	$(CC) $(LDFLAGS) -o $@ $(LFLAGS) $(SUMACLUST_OBJ) $(LIB) $(LIBSUMAPATH)
	
########
#
# project management
#
########

clean:
	rm -f $(SUMACLUST_OBJ)	
	rm -f $(EXEC)
	$(MAKE) -C ./sumalibs clean

install: all
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -m 755 $(EXEC) $(DESTDIR)$(PREFIX)/bin/

